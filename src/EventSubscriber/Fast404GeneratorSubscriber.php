<?php

namespace Drupal\fast_404_generator\EventSubscriber;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Fast 404 Generator event subscriber.
 */
class Fast404GeneratorSubscriber implements EventSubscriberInterface {

  /**
   * Event subscriber constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user object.
   */
  public function __construct(protected FileSystemInterface $fileSystem, protected AccountProxyInterface $currentUser) {
  }

  /**
   * Kernel response event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Response event.
   */
  public function onKernelResponse(ResponseEvent $event) {
    if (
      $event->getResponse()->getStatusCode() == 404 &&
      !file_exists("public://404.html") &&
      $this->currentUser->isAnonymous()
    ) {
      $exts = Settings::get('fast404_exts', '/^$/');
      preg_match($exts, $event->getRequest()->getRequestUri(), $matches);
      if (!$matches) {
        $this->fileSystem->saveData($event->getResponse()
          ->getContent(), "public://404.html", FileSystemInterface::EXISTS_REPLACE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [KernelEvents::RESPONSE => ['onKernelResponse']];
  }

}
