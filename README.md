Fast 404 Generator

This module will generate a 404.html file in the public:// directory. It can then be used in the fast_404 module
settings. Using the generated page, it will retain all the styles, menus, etc that might be adjusted on the site.

-----------------------------------------------------------------
INSTALLATION INSTRUCTIONS
-----------------------------------------------------------------

1. Install Fast 404 as provided by the documentation in that module.
2. Enable this module from the `/admin/modules` page
3. Adjust the Fast 404 settings file with the following:
```
$site_404 = DRUPAL_ROOT . '/' . $site_path . '/files/404.html';
$settings['fast404_HTML_error_page'] = file_exists($site_404) ? $site_404 : FALSE;
$settings['fast404_path_check'] = file_exists($site_404);
```
